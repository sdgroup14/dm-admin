import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from 'ng-click-outside';
import {SelectTemplateComponent} from '../content/templates/select/select-template.component';
import {SwitchComponent} from '../content/templates/switch/switch.component';
import {LangsPanelComponent} from '../content/templates/langs-panel/langs-panel.component';
import {BasketDropdownComponent} from '../content/templates/basket-dropdown/basket-dropdown.component';
import {HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import {ImageCropperModule} from 'ngx-image-cropper';
import {CropImageComponent} from '../content/templates/crop-image/crop-image.component';
import {InputFileImgComponent} from '../content/templates/input-file-img/input-file-img.component';
import {ColorsDropdownComponent} from '../content/templates/colors-dropdown/colors-dropdown.component';
import {DropdownAutocompleteComponent} from '../content/templates/dropdown-autocomplete/dropdown-autocomplete.component';
import {SimpleInputFileImgComponent} from '../content/templates/simple-input-file-img/simple-input-file-img.component';
import {DropdownAutocompleteService} from '../content/templates/dropdown-autocomplete/dropdown-autocomplete.service';
import {SpinnerBtnComponent} from '../content/templates/spinner-btn/spinner-btn.component';
import {ColorsDropdown2Component} from '../content/templates/colors-dropdown2/colors-dropdown2.component';
import {SortPipe} from '../pipes/sort.pipe';
import {SafeUrlPipe} from '../pipes/safe-url.pipe';
import {DropdownAutocompleteCityComponent} from '../content/templates/dropdownSearchAutoCompleteCity/dropdown-autocomplete-city.component';
import {DropdownAutocompleteCityService} from '../content/templates/dropdownSearchAutoCompleteCity/dropdown-autocomplete-city.service';
import {InterfaceTooltipComponent} from '../content/templates/interface-tooltip/interface-tooltip.component';

@NgModule({
  declarations: [
    SelectTemplateComponent,
    SwitchComponent,
    LangsPanelComponent,
    BasketDropdownComponent,
    CropImageComponent,
    InputFileImgComponent,
    ColorsDropdownComponent,
    ColorsDropdown2Component,
    SortPipe,
    SafeUrlPipe,
    DropdownAutocompleteComponent,
    DropdownAutocompleteCityComponent,
    SimpleInputFileImgComponent,
    SpinnerBtnComponent,
    InterfaceTooltipComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    HttpClientModule,
    NgxPaginationModule,
    ImageCropperModule
  ],
  providers: [DropdownAutocompleteService, DropdownAutocompleteCityService],
  exports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    SelectTemplateComponent,
    SwitchComponent,
    LangsPanelComponent,
    BasketDropdownComponent,
    HttpClientModule,
    NgxPaginationModule,
    ImageCropperModule,
    CropImageComponent,
    InputFileImgComponent,
    ColorsDropdownComponent,
    ColorsDropdown2Component,
    SortPipe,
    SafeUrlPipe,
    DropdownAutocompleteComponent,
    DropdownAutocompleteCityComponent,
    SimpleInputFileImgComponent,
    SpinnerBtnComponent,
    InterfaceTooltipComponent
  ]
})
export class SheredModule {
}
