import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {Router} from '@angular/router';
import {NotificationService} from '../../../templates/notification/notification.service';
import {Subscription} from 'rxjs';
import {CommonService} from '../../../../services/common.service';
import {DateService} from '../../../../services/date.service';
import {PersonsService} from '../services/persons.service';

@Component({
  selector: 'app-persons-root',
  templateUrl: './persons-root.component.html',
  styleUrls: ['./persons-root.component.scss']
})
export class PersonsRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;

  filterParams: any = {
    page: 0,
    sortby: 'sirname_ru',
    search: '',
    direction: 'asc',
    role: ''
  };

  sortList: any = [
    {
      text: 'ФИО',
      value: 'sirname_ru',
      reverse: false
    },
    {
      text: 'Должность',
      value: 'position_ru'
    },
    {
      text: 'Партия',
      value: 'party_id'
    },
    {
      text: 'Город',
      value: 'city_id'
    }
  ];

  persons: any = [];

  changePageHandler(page) {
    this.filterParams.page = page;
    this.service.getPersons(this.filterParams);
  }

  onSearch() {
    this.service.getPersons(this.filterParams);
  }

  onChoice(val) {
    if (val) {
      this.filterParams.party_id = val;
    } else {
      delete this.filterParams.party_id;
    }
    this.service.getPersons(this.filterParams);
  }

  deletePerson(author) {
    this.subscriptions.add(this.service.deletePersonApi(author.id).subscribe((resp: any) => {
      if (resp.success) {
        const index = this.persons.indexOf(author);
        this.persons.splice(index, 1);
        this.directiveRef.update();
        this.notif.change('success', resp.message);
      }
    }, error => {
      this.notif.change('error', error.error.message);
    }));
  }

  setOrder(sort) {
    sort.reverse = !sort.reverse;
    this.sortList.map(_sort => {
      if (sort.value !== _sort.value) {
        _sort.reverse = false;
      }
    });
    this.filterParams.direction = sort.reverse ? 'desc' : 'asc';
    this.filterParams.sortby = sort.value;
    this.service.getPersons(this.filterParams);
  }

  personEdit(author, event) {
    if (!event.target.offsetParent.classList.contains('switch')) {
      this.router.navigate(['/persons/', author.id]);
    }
  }

  constructor(private service: PersonsService,
              private router: Router,
              public date_s: DateService,
              private _common: CommonService,
              private notif: NotificationService) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.service.getPersons(this.filterParams);
    this.subscriptions.add(this.service.persons.subscribe((resp: any) => {
      console.log(resp);
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.persons = resp.result.data;
    }));
  }

}
