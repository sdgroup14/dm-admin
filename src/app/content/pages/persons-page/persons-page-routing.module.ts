import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonsAddComponent} from './persons-add/persons-add.component';
import {PersonsRootComponent} from './persons-root/persons-root.component';

const routes: Routes = [
  {
    path: '',
    component: PersonsRootComponent
  },
  {
    path: 'add',
    component: PersonsAddComponent
  },
  {
    path: ':id',
    component: PersonsAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonsPageRoutingModule { }
