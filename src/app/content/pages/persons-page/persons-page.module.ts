import {NgModule} from '@angular/core';
import {SheredModule} from '../../../modules/shered.module';
import {PersonsPageRoutingModule} from './persons-page-routing.module';
import {PersonsService} from './services/persons.service';
import {PersonsRootComponent} from './persons-root/persons-root.component';
import {PersonsAddComponent} from './persons-add/persons-add.component';
import {DateAdapter, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule} from '@angular/material';

@NgModule({
  declarations: [PersonsRootComponent, PersonsAddComponent],
  imports: [
    PersonsPageRoutingModule,
    SheredModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule
  ],
  providers: [PersonsService]
})
export class PersonsPageModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    // dateAdapter.setLocale('cv'); // DD/MM/YYYY

  }
}
