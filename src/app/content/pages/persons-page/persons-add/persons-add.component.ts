import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../templates/notification/notification.service';
import {Subscription} from 'rxjs';
import {CommonService} from '../../../../services/common.service';
import {SpinnerBtnService} from '../../../templates/spinner-btn/spinner-btn.service';
import {PersonsService} from '../services/persons.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
// import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
// import {_rollupMoment} from 'moment';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

// const moment = _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-authors-add',
  templateUrl: './persons-add.component.html',
  styleUrls: ['./persons-add.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PersonsAddComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  cropImg: any;
  cropIsOpen = false;
  cropOptions: any = {};
  photoChek: any = null;
  pageTitle = '';
  btnText = '';
  newAuthor = true;
  date;
  langsSirname = [
    {
      selected: true,
      value: 'sirname_ru',
      text: 'рус'
    },
    {
      selected: false,
      value: 'sirname_ua',
      text: 'укр'
    }
  ];
  langsWork = [
    {
      selected: true,
      value: 'work_ru',
      text: 'рус'
    },
    {
      selected: false,
      value: 'work_ua',
      text: 'укр'
    }
  ];
  langsName = [
    {
      selected: true,
      value: 'name_ru',
      text: 'рус'
    },
    {
      selected: false,
      value: 'name_ua',
      text: 'укр'
    }
  ];
  langsPosition = [
    {
      selected: true,
      value: 'position_ru',
      text: 'рус'
    },
    {
      selected: false,
      value: 'position_ua',
      text: 'укр'
    }
  ];

  photoOptions: any = {
    formats: ['png', 'jpg', 'jpeg', 'bmp', 'gif'],
    maxSize: 99999999999999,
    aspectRatio: 1 / 1,
    type: 'photo',
    maintainAspectRatio: true
  };

  errors: any = {};

  person: any = {};

  passdata: any = {};
  passerror: any = {};

  addPerson() {
    for (const option in this.person) {
      if (this.person[option] === null) {
        delete this.person[option];
      }
    }

    const date = new Date(this.date);
    // console.log(this.timeValues.date);
    const utc_timestamp = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
    this.person.birthday = utc_timestamp / 1000;

    this.subscriptions.add(this.service.addPersonApi(this.person).subscribe((resp: any) => {
      this.btnSpinner.hide();
      console.log(this.person);
      if (!this.person.hasOwnProperty('id')) {
        this.router.navigate(['/persons/', resp.result.id]);
      }
      this.notif.change('success', resp.message);
    }, error => {
      this.btnSpinner.hide();
      const data = error.error;
      this.notif.change('error', data.message);
      for (const _error in data.errors) {
        this.errors[_error] = data.errors[_error];
      }
    }));
  }

  cropImage(data) {
    if (this.photoChek) {
      this.person.photo = data;
    } else {
      this.person.avatar = data;
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: PersonsService,
              private notif: NotificationService,
              public _common: CommonService,
              private route: ActivatedRoute,
              private btnSpinner: SpinnerBtnService,
              private router: Router) {
  }

  // getTime(article) {
  //   const date = new Date(article.publish_time * 1000);
  //   const utc_timestamp = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
  //   this.timeValues.date = utc_timestamp;
  // }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params['id']) {
          this.newAuthor = false;
          this.pageTitle = 'Изменить';
          this.btnText = 'Сохранить';
          this.subscriptions.add(this.service.getPersonApi(params['id']).subscribe((resp: any) => {
            if (resp) {
              this.person = resp.result;
              this.person.city = {
                id: resp.result.city_id,
                name: resp.result.city_name
              };
              const date = new Date(resp.result.birthday * 1000);
              const utc_timestamp = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
              this.date = utc_timestamp;
            }
          }));
        } else {
          this.pageTitle = 'Новая персона';
          this.btnText = 'Добавить';
          this.newAuthor = true;
        }
      })
    );
  }

}
