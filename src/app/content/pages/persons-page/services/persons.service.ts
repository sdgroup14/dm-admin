import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  public persons = new Subject();

  constructor(private http: HttpClient) {
  }

  getPersons(params) {
    let urlParams = '?';
    urlParams += 'sortby=' + params.sortby;
    urlParams += params.page ? '&page=' + params.page : '';
    urlParams += params.search !== '' ? '&search=' + params.search.toLowerCase() : '';
    urlParams += '&direction=' + params.direction;
    urlParams += params.party_id ? '&party_id=' + params.party_id : '';
    urlParams += params.role !== '' ? '&role=' + params.role : '';
    // console.log(urlParams);
    return this.http.get(environment.apiUrl + '/person' + urlParams, {}).subscribe(resp => {
      this.persons.next(resp);
    });
  }

  deletePersonApi(id) {
    return this.http.delete(environment.apiUrl + '/person/' + id, {});
  }

  getPersonApi(id) {
    return this.http.get(environment.apiUrl + '/person/id/' + id, {});
  }

  addPersonApi(person) {
    const apiOptions = {};
    const apiData = new FormData();
    for (const option in person) {
      if (option === 'photo' && (typeof person[option]) ! === 'string') {

      } else if (option === 'avatar' && (typeof person[option]) ! === 'string') {

      } else {
        if (person[option] !== '' && person[option] !== null) {
          apiData.append(option, person[option]);
        }
      }
    }

    let id = '';
    if (person.hasOwnProperty('id')) {
      id = person.id;
      return this.http.post(environment.apiUrl + '/person/update/' + id, apiData, apiOptions);
    }
    return this.http.post(environment.apiUrl + '/person', apiData, apiOptions);
  }


}
