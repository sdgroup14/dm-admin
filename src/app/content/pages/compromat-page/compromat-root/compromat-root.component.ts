import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {Router} from '@angular/router';
import {CommonService} from '../../../../services/common.service';
import {DateService} from '../../../../services/date.service';
import {NotificationService} from '../../../templates/notification/notification.service';
import {CompromatService} from '../services/compromat.service';

@Component({
  selector: 'app-compromat-root',
  templateUrl: './compromat-root.component.html',
  styleUrls: ['./compromat-root.component.scss']
})
export class CompromatRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;

  filterParams: any = {
    page: 0,
    author: '',
    search: '',
    role: '',
    direction: 'desc',
    sortby: 'pub_time'
  };

  pagginationOptions: any = {};

  sortList: any = [
    {
      text: 'Заголовок',
      value: 'title_ru'
    },
    {
      text: 'Про кого',
      value: 'author'
    },
    {
      text: 'Время',
      value: 'pub_time',
      reverse: true
    }
  ];

  compromats: any = [];

  changePageHandler(page) {
    this.filterParams.page = page;
    this.service.getAllCompromats(this.filterParams);
  }

  deleteArticle(article) {
    this.subscriptions.add(this.service.deleteApi(article.id).subscribe((resp: any) => {
      if (resp.success) {
        const index = this.compromats.indexOf(article);
        this.compromats.splice(index, 1);
        this.directiveRef.update();
        this.notif.change('success', resp.message);
      }
    }, error => {
      this.notif.change('error', error.error.message);
    }));
  }

  edit(article, event) {
    if (!event.target.offsetParent.classList.contains('switch')) {
      this.router.navigate(['/compromat/', article.id]);
    }
  }

  setOrder(sort) {
    if (sort.value !== 'author') {
      sort.reverse = !sort.reverse;
      this.sortList.map(_sort => {
        if (sort.value !== _sort.value) {
          _sort.reverse = false;
        }
      });
      this.filterParams.direction = sort.reverse ? 'desc' : 'asc';
      this.filterParams.sortby = sort.value;
      this.service.getAllCompromats(this.filterParams);
    }

  }

  constructor(
    private service: CompromatService,
    private router: Router,
    private _common: CommonService,
    public date_s: DateService,
    private notif: NotificationService
  ) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.service.getAllCompromats(this.filterParams);
    this.subscriptions.add(this.service.compromats.subscribe((resp: any) => {
      this.pagginationOptions.itemsPerPage = resp.result.per_page;
      this.pagginationOptions.total = resp.result.total;

      this.compromats = resp.result.data;
    }));
  }

}
