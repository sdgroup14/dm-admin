import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CompromatRootComponent} from './compromat-root/compromat-root.component';
import {CompromatArticleComponent} from './compromat-add/compromat-article/compromat-article.component';
import {CompromatHeadComponent} from './compromat-add/compromat-head/compromat-head.component';
import {CompromatSeoComponent} from './compromat-add/compromat-seo/compromat-seo.component';
import {CompromatAddComponent} from './compromat-add/compromat-add.component';

const routes: Routes = [
  {
    path: '',
    component: CompromatRootComponent
  },
  {
    path: 'add',
    component: CompromatAddComponent,
    children: [
      {
        path: 'head',
        component: CompromatHeadComponent
      },
      {
        path: 'blocks',
        component: CompromatArticleComponent
      },
      {
        path: 'seo',
        component: CompromatSeoComponent
      },
      {
        path: '',
        pathMatch: 'prefix',
        redirectTo: 'head'
      }
    ]
  },
  {
    path: ':id',
    component: CompromatAddComponent,
    children: [
      {
        path: 'head',
        component: CompromatHeadComponent
      },
      {
        path: 'blocks',
        component: CompromatArticleComponent
      },
      {
        path: 'seo',
        component: CompromatSeoComponent
      },
      {
        path: '',
        pathMatch: 'prefix',
        redirectTo: 'head'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompromatPageRoutingModule { }
