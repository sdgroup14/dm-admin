import {NgModule} from '@angular/core';

import {CompromatPageRoutingModule} from './compromat-page-routing.module';
import {SheredModule} from '../../../modules/shered.module';
import {CompromatService} from './services/compromat.service';
import {CompromatRootComponent} from './compromat-root/compromat-root.component';
import {CompromatAddComponent} from './compromat-add/compromat-add.component';
import {CompromatArticleComponent} from './compromat-add/compromat-article/compromat-article.component';
import {CompromatSeoComponent} from './compromat-add/compromat-seo/compromat-seo.component';
import {CompromatHeadComponent} from './compromat-add/compromat-head/compromat-head.component';
import {DateAdapter, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule} from '@angular/material';
import {DndModule} from 'ng2-dnd';
import {EditorModule} from '@tinymce/tinymce-angular';
import {SafeHtmlPipe} from '../../../pipes/safe-html.pipe';

@NgModule({
  declarations: [
    CompromatRootComponent,
    CompromatAddComponent,
    CompromatSeoComponent,
    CompromatHeadComponent,
    CompromatArticleComponent,
    SafeHtmlPipe
  ],
  imports: [
    SheredModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    CompromatPageRoutingModule,
    DndModule.forRoot(),
    EditorModule
  ],
  providers: [CompromatService]
})
export class CompromatPageModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    // dateAdapter.setLocale('en-in'); // DD/MM/YYYY
  }
}
