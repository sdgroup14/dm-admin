import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompromatService {
  public compromats = new Subject();
  public block = new Subject();
  public compromat = new BehaviorSubject(null);
  public click = new Subject();

  constructor(private http: HttpClient) {
  }

  getAllCompromats(params: any) {
    let urlParams = '';
    let index = 0;
    for (const param in params) {
      if (params[param] !== '' && params[param]) {
        if (index === 0) {
          urlParams += '?' + param + '=' + params[param];
        } else {
          urlParams += '&' + param + '=' + params[param];
        }
        index++;
      }
    }

    return this.http.get(environment.apiUrl + '/dirt' + urlParams, {}).subscribe(resp => {
      this.compromats.next(resp);
    });
  }

  deleteApi(id) {
    return this.http.delete(environment.apiUrl + '/dirt/' + id, {});
  }

  getCompromatApi(id) {
    return this.http.get(environment.apiUrl + '/dirt/id/' + id, {});
  }

  addCompromatApi(article) {
    let apiData: any;
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };

    apiData = new FormData();
    for (const option in article) {
      if (article[option] !== '' || article[option]) {
        apiData.append(option, article[option]);
      }
    }

    let id = '';
    if (article.hasOwnProperty('id')) {
      id = article.id;
      return this.http.post(environment.apiUrl + '/dirt/update/' + id, apiData, apiOptions);
    }
    return this.http.post(environment.apiUrl + '/dirt/', apiData, apiOptions);
  }

  blockApi(block) {
    // console.log(block)
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };
    const apiData = new FormData();
    for (const option in block) {
      if (option !== 'datepick'
        && option !== 'langs' &&
        option !== 'langs1' &&
        option !== 'slides' &&
        option !== 'photoOptions' &&
        option !== 'data' &&
        option !== 'search' &&
        option !== 'articles' &&
        option !== 'choice' &&
        option !== 'errors' &&
        option !== 'content') {
        if (option === 'img' && typeof block[option] === 'string') {

        } else if (option === 'video' && typeof block[option] === 'string') {

        } else
        if (option === 'title_ru' && block.type === 25) {

        } else {
          if (block[option] !== '') {
            apiData.append(option, block[option]);
          }
        }
        // console.log(block[option]);

      }
    }
    // apiData.append('article_id', '2');
    // apiData.append('order_number', '1');
    return this.http.post(environment.apiUrl + '/blocks', apiData, apiOptions).subscribe(resp => {
      this.block.next(resp);
    }, err => {
      this.block.next(err);
    });
  }

  deleteBlockApi(id) {
    return this.http.delete(environment.apiUrl + '/blocks/' + id, {});
  }

  videoApi(data) {
    // console.log(block)
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };
    const apiData = new FormData();

    for (const option in data) {
      apiData.append(option, data[option]);
    }
    // apiData.append('photo', slide.photo);
    // apiData.append('index', slide.index);
    // apiData.append('order_number', slide.order_number);
    // apiData.append('article_id', slide.article_id);
    // apiData.append('type', slide.type);
    return this.http.post(environment.apiUrl + '/video', apiData, apiOptions);
  }
  audioApi(data) {
    // console.log(block)
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };
    const apiData = new FormData();

    for (const option in data) {
      apiData.append(option, data[option]);
    }
    // apiData.append('photo', slide.photo);
    // apiData.append('index', slide.index);
    // apiData.append('order_number', slide.order_number);
    // apiData.append('article_id', slide.article_id);
    // apiData.append('type', slide.type);
    return this.http.post(environment.apiUrl + '/audio', apiData, apiOptions);
  }

  deleteVideo(id) {
    return this.http.delete(environment.apiUrl + '/video/' + id, {});
  }

  deleteAudio(id) {
    return this.http.delete(environment.apiUrl + '/audio/' + id, {});
  }




}
