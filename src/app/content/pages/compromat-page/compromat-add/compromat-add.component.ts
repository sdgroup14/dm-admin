import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {NotificationService} from '../../../templates/notification/notification.service';
import {ActivatedRoute} from '@angular/router';
import {CompromatService} from '../services/compromat.service';

@Component({
  selector: 'app-compromat-add',
  templateUrl: './compromat-add.component.html',
  styleUrls: ['./compromat-add.component.scss']
})
export class CompromatAddComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;
  isCreate = true;
  pageTitle = '';
  btnText = '';
  tabs: any = [
    {
      text: 'Шапка',
      show: false,
      link: 'head'
    },
    {
      text: 'Статья',
      show: false,
      link: 'blocks'
    },
    // {
    //   text: 'SEO',
    //   show: false,
    //   link: 'seo'
    // }
  ];

  copyText(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.notif.change('success', 'текст помещен в буфер обмена');
  }


  addArticle() {
    this.service.click.next(true);
  }


  constructor(private service: CompromatService,
              private notif: NotificationService,
              private route: ActivatedRoute) {
  }

  ngOnDestroy() {
    this.service.compromat.next(null);
    this.subscriptions.unsubscribe();
  }

  articleId: any;
  slug: any;

  ngOnInit() {
    // setTimeout(()=> {
    //   this.directiveRef.update();
    //   console.log('update')
    // }, 5000)
    this.subscriptions.add(this.route.params.subscribe(params => {
      if (params['id']) {
        this.isCreate = false;
        this.pageTitle = 'Изменить';
        this.btnText = 'Сохранить';
        this.subscriptions.add(this.service.getCompromatApi(params['id']).subscribe((resp: any) => {
          this.articleId = resp.result.id;
          this.slug = resp.result.slug;
          this.service.compromat.next(resp);
        }));
      } else {
        this.pageTitle = 'Новая статья';
        this.btnText = 'Сохранить';
      }
    }));
  }
}
