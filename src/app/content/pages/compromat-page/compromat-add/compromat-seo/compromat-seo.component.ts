import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {NotificationService} from '../../../../templates/notification/notification.service';
import {CommonService} from '../../../../../services/common.service';
import {SpinnerBtnService} from '../../../../templates/spinner-btn/spinner-btn.service';
import {CompromatService} from '../../services/compromat.service';

@Component({
  selector: 'app-compromat-seo',
  templateUrl: './compromat-seo.component.html',
  styleUrls: ['./compromat-seo.component.scss']
})
export class CompromatSeoComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  compromat: any = {};
  errors: any = {};
  cropIsOpen = false;
  cropOptions: any = {};
  cropImg: any;
  photoOptions: any = {
    formats: ['png', 'jpg', 'jpeg', 'bmp', 'tif', 'gif'],
    maxSize: 999999999999999,
    aspectRatio: 1200 / 630,
    type: 'fb_cover',
    maintainAspectRatio: true
  };
  seo: any = {};
  descriptions = [
    {
      selected: true,
      value: 'descr_ru',
      maxlength: '170',
      text: 'рус'
    },
    {
      selected: false,
      value: 'descr_ua',
      maxlength: '170',
      text: 'укр'
    }
  ];

  seoTitle = [
    {
      selected: true,
      value: 'seo_title_ru',
      maxlength: '170',
      text: 'рус'
    },
    {
      selected: false,
      value: 'seo_title_ua',
      maxlength: '170',
      text: 'укр'
    }
  ];

  keywords = [
    {
      selected: true,
      value: 'keywords_ru',
      maxlength: '1000',
      text: 'рус'
    },
    {
      selected: false,
      value: 'keywords_ua',
      maxlength: '1000',
      text: 'укр'
    }
  ];

  constructor(
    private notif: NotificationService,
    public _common: CommonService,
    private btnSpinner: SpinnerBtnService,
    private service: CompromatService) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.service.compromat.subscribe(resp => {
      if (resp) {
        this.compromat = resp.result;
        this.seo.descr_ru = this.compromat.descr_ru;
        this.seo.descr_ua = this.compromat.descr_ua;
        this.seo.keywords_ru = this.compromat.keywords_ru;
        this.seo.keywords_ua = this.compromat.keywords_ua;
        this.seo.seo_title_ru = this.compromat.seo_title_ru;
        this.seo.seo_title_ua = this.compromat.seo_title_ua;
        this.seo.fb_cover = this.compromat.fb_cover;
      }
    }));


    this.subscriptions.add(this.service.click.subscribe(() => {
      this.seo.article_id = this.compromat.id;
      // console.log(this.seo)
      for (const option in this.seo) {
        if (this.seo[option] === null || this.seo[option] === '' || this.seo[option] === undefined) {
          delete this.seo[option];
        }
      }

      // this.subscriptions.add(this.service.seokApi(this.seo).subscribe((resp: any) => {
      //   this.btnSpinner.hide();
      //   if (resp.success) {
      //     this.compromat.descr_ru = this.seo.descr_ru;
      //     this.compromat.descr_ua = this.seo.descr_ua;
      //     this.compromat.keywords_ru = this.seo.keywords_ru;
      //     this.compromat.keywords_ua = this.seo.keywords_ua;
      //     this.compromat.seo_title_ru = this.seo.seo_title_ru;
      //     this.compromat.seo_title_ua = this.seo.seo_title_ua;
      //
      //     // console.log(this.seo.fb_cover)
      //     if(this.seo.fb_cover){
      //       this.compromat.fb_cover = this.seo.fb_cover;
      //     }
      //
      //     this.notif.change('success', resp.message);
      //   }
      // }, error => {
      //   this.btnSpinner.hide();
      //   const data = error.error;
      //   this.notif.change('error', data.message);
      //   for (const _error in data.errors) {
      //     this.errors[_error] = data.errors[_error];
      //   }
      // }));
    }));
  }
}
