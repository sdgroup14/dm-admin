import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {SpinnerBtnService} from '../../../../templates/spinner-btn/spinner-btn.service';
import {NotificationService} from '../../../../templates/notification/notification.service';
import {CommonService} from '../../../../../services/common.service';
import {Subscription} from 'rxjs';
import {CompromatService} from '../../services/compromat.service';

@Component({
  selector: 'app-compromat-article',
  templateUrl: './compromat-article.component.html',
  styleUrls: ['./compromat-article.component.scss']
})
export class CompromatArticleComponent implements OnInit, OnDestroy {

  constructor(private service: CompromatService,
              private sanitizer: DomSanitizer,
              private _ngZone: NgZone,
              private btnSpinner: SpinnerBtnService,
              private notif: NotificationService,
              public _common: CommonService
  ) {
  }

  // public Editor = BalloonEditor;
  private subscriptions = new Subscription();
  cropIsOpen = false;
  cropImg: any;
  cropOptions: any = {};
  currentItem: any = null;
  file_index = 0;
  files_total: any;
  fileProgressInit = false;
  photoOptions: any = {
    formats: ['png', 'jpg', 'jpeg', 'bmp', 'gif'],
    maxSize: 999999999999999,
    aspectRatio: 1920 / 822,
    maintainAspectRatio: false,
    type: 'img'
  };
  typesOpen = false;
  compromat: any = {
    blocks: []
  };

  editorOptions: any;

  blockIndex = 0;

  // twitterUpd() {
  //   setTimeout(() => {
  //     twttr.widgets.load();
  //   });
  // }
  //
  // instUpd() {
  //   setTimeout(() => {
  //     instgrm.Embeds.process();
  //   });
  // }


  fastImg(data, item) {
    if (data.type.search('gif')) {
      item.img = data;
    }
  }

  pushToList(index, arr, event) {
    arr.splice(index + 1, 0, {});
    setTimeout(() => {
      event.target.offsetParent.nextSibling.children[0].focus();
    });
  }

  deleteFromList(index, arr, event, input) {
    if (!input.value) {
      event.target.offsetParent.previousSibling.children[0].focus();
      arr.splice(index, 1);
    }
  }

  addNewItem(type) {
    let langs = [];
    let langs1 = [];
    let photoOptions = {};
    if (type === 1) {
      langs = [
        {
          selected: true,
          value: 'h2_ru',
          text: 'рус'
        },
        {
          selected: false,
          value: 'h2_ua',
          text: 'укр'
        }
      ];
    } else if (type === 2) {
      langs = [
        {
          selected: true,
          value: 'text_ru',
          text: 'рус'
        },
        {
          selected: false,
          value: 'text_ua',
          text: 'укр'
        }
      ];
    } else if (type === 3) {
      langs = [
        {
          selected: true,
          value: 'caption_ru',
          text: 'рус'
        },
        {
          selected: false,
          value: 'caption_ua',
          text: 'укр'
        }
      ];
      photoOptions = {
        formats: ['png', 'jpg', 'jpeg', 'bmp', 'tif', 'gif'],
        maxSize: 999999999999999,
        aspectRatio: 320 / 320,
        maintainAspectRatio: false,
        type: 'img'
      };
    } else if (type === 4) {
      langs = [
        {
          selected: true,
          value: 'caption_ru',
          text: 'рус'
        },
        {
          selected: false,
          value: 'caption_ua',
          text: 'укр'
        }
      ];
      photoOptions = {
        formats: ['mp4'],
        maxSize: 102400000,
        aspectRatio: 1 / 1,
        maintainAspectRatio: false,
        minHeight: null,
        minWidth: null,
        type: 'img'
      };
    } else if (type === 5) {
      langs = [
        {
          selected: true,
          value: 'caption_ru',
          text: 'рус'
        },
        {
          selected: false,
          value: 'caption_ua',
          text: 'укр'
        }
      ];
      photoOptions = {
        formats: ['mp3'],
        maxSize: 102400000,
        aspectRatio: 1 / 1,
        maintainAspectRatio: false,
        minHeight: null,
        minWidth: null,
        type: 'img'
      };
    }


    const _block: any = {
      order: null,
      type: type,
      errors: {},
      content: {},
      langs: langs,
      langs1: langs1,
      photoOptions: photoOptions,
      datepick: {
        date: null,
        hours: '00',
        minutes: '00'
      }
    };
// console.log(_block)
    this.compromat.blocks.push(_block);
    // console.log(_block);
    this.compromat.blocks.map((_item, _i) => {
      _item.order = _i;
    });
  }

  deleteSlide(slide, slides) {
    // this.service.deleteSlide(slide.id).subscribe(resp => {
    //   const index = slides.indexOf(slide);
    //   slides.splice(index, 1);
    //   this.notif.change('success', 'слайд удален');
    // });
  }

  setSlideOrder(item) {
    const new_orders = [];
    item.slides.map((slide, i) => {
      slide.index = i;
      new_orders.push({
        id: slide.id,
        index: slide.index
      });
    });

    // this.service.orderSlides(new_orders).subscribe(resp => {
    //   this.notif.change('success', 'Слайды пересортированы');
    // });
  }

  checkTime(e, inp, maxval) {
    if (+inp > maxval) {
      e.target.value = maxval;
      inp = maxval;
    }
  }

  uploadVideo(file, item) {
    const api_video: any = {
      video: file,
      order: item.order,
      dirt_id: this.compromat.id,
      type: item.type
    };
    if (item.id) {
      api_video.block_id = item.id;
    }
    item.isDownload = true;
    item.isDownloadText = 'Идет загрузка видео';
    let count = 0;
    const interval = setInterval(() => {
      if (!count) {
        item.isDownloadText = 'Идет загрузка видео';
      } else if (count === 1) {
        item.isDownloadText = 'Идет загрузка видео.';
      } else if (count === 2) {
        item.isDownloadText = 'Идет загрузка видео..';
      } else if (count === 3) {
        item.isDownloadText = 'Идет загрузка видео...';
        count = 0;
        return;
      }
      count++;
    }, 500);
    // console.log(api_video);
    this.service.videoApi(api_video).subscribe((resp: any) => {
      item.id = resp.result.block_id;
      item.video = resp.result.video;
      item.isDownload = false;
      clearInterval(interval);
      item.isDownloadText = 'Идет загрузка видео...';
    });
  }

  uploadAudio(file, item) {
    console.log('123')
    const api_audio: any = {
      audio: file,
      order: item.order,
      dirt_id: this.compromat.id,
      type: item.type
    };
    if (item.id) {
      api_audio.block_id = item.id;
    }
    item.isDownload = true;
    item.isDownloadText = 'Идет загрузка аудио';
    let count = 0;
    const interval = setInterval(() => {
      if (!count) {
        item.isDownloadText = 'Идет загрузка аудио';
      } else if (count === 1) {
        item.isDownloadText = 'Идет загрузка аудио.';
      } else if (count === 2) {
        item.isDownloadText = 'Идет загрузка аудио..';
      } else if (count === 3) {
        item.isDownloadText = 'Идет загрузка аудио...';
        count = 0;
        return;
      }
      count++;
    }, 500);
    // console.log(api_video);
    this.service.audioApi(api_audio).subscribe((resp: any) => {
      item.id = resp.result.block_id;
      item.audio = resp.result.audio;
      item.isDownload = false;
      clearInterval(interval);
      item.isDownloadText = 'Идет загрузка видео...';
      console.log(item)
    });
  }



  addSlide(e, item: any) {
    const files = e.target.files;
    if (files.length) {
      const fileExtension = files[this.file_index].name.replace(/^.*\./, '');
      const checkFormat = this.photoOptions.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (+files[this.file_index].size > +this.photoOptions.maxSize) {
          this.notif.change('error', files[this.file_index].name + ' больше 8мб');
        } else {
          const reader = new FileReader();
          reader.onload = (_e: any) => {
            const img = new Image();
            img.src = _e.target.result;
            this.files_total = files.length;
            this.fileProgressInit = true;
            img.onload = () => {
              // setTimeout(() => {
              const api_slide: any = {
                photo: files[this.file_index],
                index: item.slides.length,
                order_number: item.order_number,
                article_id: this.compromat.id,
                type: item.type
              };
              if (item.id) {
                api_slide.block_id = item.id;
              }
              // this.service.slideApi(api_slide).subscribe((resp: any) => {
              //   setTimeout(() => {
              //     item.id = resp.result.block_id;
              //     item.slides.push({
              //       id: resp.result.slide.id,
              //       index: resp.result.slide.index,
              //       photo: resp.result.slide.photo
              //     });
              //     this.file_index++;
              //     if (this.file_index < files.length) {
              //       this.addSlide(e, item);
              //     } else {
              //       this.fileProgressInit = false;
              //       this.files_total = null;
              //       this.file_index = 0;
              //       e.target.value = '';
              //     }
              //   }, 100);
              // });

            };
          };
          reader.readAsDataURL(files[this.file_index]);
        }
      } else {
        this.notif.change('error', 'не правильный формат файла ' + files[this.file_index].name);
      }
    }
  }

  // autocompleteArticle(item) {
  //   if (item.search.length > 0) {
  //     this.service.searchArticle(item.search).subscribe((resp: any) => {
  //       item.articles = resp.result;
  //     });
  //   } else {
  //     item.articles = [];
  //   }
  // }
  //
  // addDropdownArticle(article, item) {
  //   item.choice = article;
  //   item.search = '';
  //   item.articles = [];
  // }
  //
  // deleteArticleId(item) {
  //   item.choice = null;
  //   item.search = '';
  //   item.articles = [];
  // }

  changeOrder(item, i, check) {
    if (check) {
      this.compromat.blocks.splice(i - 1, 0, item);
      this.compromat.blocks.splice(i + 1, 1);
    } else {
      this.compromat.blocks.splice(i + 2, 0, item);
      this.compromat.blocks.splice(i, 1);
    }
    this.compromat.blocks.map((_item, _i) => {
      _item.order = _i;
    });
    console.log(this.compromat.blocks);
  }

  changeOrderDrag() {
    this.compromat.blocks.map((_item, _i) => {
      _item.order = _i;
    });
  }

  deleteVideo(item) {
    this.subscriptions.add(this.service.deleteVideo(item.id).subscribe((resp: any) => {
      if (resp.success) {
        delete item.video;
        this.notif.change('success', resp.message);
      }
    }, error => {
      this.notif.change('error', error.error.message);
    }));
  }
  deleteAudio(item) {
    this.subscriptions.add(this.service.deleteAudio(item.id).subscribe((resp: any) => {
      if (resp.success) {
        delete item.audio;
        this.notif.change('success', resp.message);
      }
    }, error => {
      this.notif.change('error', error.error.message);
    }));
  }


  onReady(editor) {
    // editor.config.plugins = [Underline];
    // editor.config.toolbar = ['underline']
    // .create(document.querySelector('ckeditor'), {
    //   plugins: [Underline],
    //   img: {
    //     toolbar: ['underline']
    //   }
    // });
    // editor.ui.view.editable.element.parentElement.insertBefore(
    //   editor.ui.view.toolbar.element,
    //   editor.ui.view.editable.element
    // );
  }

  deleteBlock(item) {
    if (item.id) {
      this.subscriptions.add(this.service.deleteBlockApi(item.id).subscribe((resp: any) => {
        if (resp.success) {
          const index = this.compromat.blocks.indexOf(item);
          this.compromat.blocks.splice(index, 1);
          this.notif.change('success', resp.message);
        }
      }, error => {
        this.notif.change('error', error.error.message);
      }));
    } else {
      const index = this.compromat.blocks.indexOf(item);
      this.compromat.blocks.splice(index, 1);
    }
  }

  closeList(e) {
    if (!e.target.classList.contains('types_btn')) {
      this.typesOpen = false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {


    this.editorOptions = {
      theme: 'inlite',
      inline: true,
      selection_toolbar: 'bold, quicklink',
      // selection_toolbar: 'bold, italic, quicklink',
      insert_toolbar: '',
      plugins: 'paste',
      paste_as_text: true,
      setup: (editor) => {
        editor.on('change', (e) => {
          const links = e.target.targetElm.getElementsByTagName('a');
          if (links.length) {
            for (let i = 0; i < links.length; i++) {
              links[i].target = '_blank';
            }
          }
        });
      }
    };
    this.subscriptions.add(this.service.compromat.subscribe((resp: any) => {
      if (resp) {
        resp.result.blocks = resp.result.blocks ? resp.result.blocks : [];
        resp.result.blocks.map(block => {
          block.errors = {};
          for (const opt in block.content) {
            block[opt] = block.content[opt];
          }
          block.content = {};
          if (block.type === 1) {
            block.h2_ru = block.data.h2_ru;
            block.h2_ua = block.data.h2_ua;
            block.langs = [
              {
                selected: true,
                value: 'h2_ru',
                text: 'рус'
              },
              {
                selected: false,
                value: 'h2_ua',
                text: 'укр'
              }
            ];
          } else if (block.type === 2) {
            block.text_ru = block.data.text_ru;
            block.text_ua = block.data.text_ua;
            block.langs = [
              {
                selected: true,
                value: 'text_ru',
                text: 'рус'
              },
              {
                selected: false,
                value: 'text_ua',
                text: 'укр'
              }
            ];
          } else if (block.type === 3) {
            block.caption_ru = block.data.caption_ru;
            block.caption_ua = block.data.caption_ua;
            block.img = block.data.img;
            block.photoOptions = {
              formats: ['png', 'jpg', 'jpeg', 'bmp', 'tif', 'gif'],
              maxSize: 999999999999999,
              aspectRatio: 1920 / 822,
              maintainAspectRatio: false,
              type: 'img'
            };
            block.langs = [
              {
                selected: true,
                value: 'caption_ru',
                text: 'рус'
              },
              {
                selected: false,
                value: 'caption_ua',
                text: 'укр'
              }
            ];
          } else if (block.type === 4) {
            block.caption_ru = block.data.caption_ru;
            block.caption_ua = block.data.caption_ua;
            block.video = block.data.video;
            block.photoOptions = {
              formats: ['mp4'],
              maxSize: 102400000,
              aspectRatio: 1 / 1,
              maintainAspectRatio: true,
              minHeight: null,
              minWidth: null,
              type: 'image'
            };
            block.langs = [
              {
                selected: true,
                value: 'caption_ru',
                text: 'рус'
              },
              {
                selected: false,
                value: 'caption_ua',
                text: 'укр'
              }
            ];
          }else if (block.type === 5) {
            block.caption_ru = block.data.caption_ru;
            block.caption_ua = block.data.caption_ua;
            block.audio = block.data.audio;
            block.photoOptions = {
              formats: ['mp3'],
              maxSize: 102400000,
              aspectRatio: 1 / 1,
              maintainAspectRatio: true,
              minHeight: null,
              minWidth: null,
              type: 'image'
            };
            block.langs = [
              {
                selected: true,
                value: 'caption_ru',
                text: 'рус'
              },
              {
                selected: false,
                value: 'caption_ua',
                text: 'укр'
              }
            ];
          }

        });
        this.compromat = resp.result;
        console.log(this.compromat);

        // this.compromat.blocks.map(block => {
        //   if (block.type === 21) {
        //     setTimeout(() => {
        //       twttr.widgets.load();
        //     });
        //   } else if (block.type === 23) {
        //     setTimeout(() => {
        //       instgrm.Embeds.process();
        //     });
        //   }
        // });

        // this.compromat.blocks = this.compromat.blocks;
        // if (this.compromat.color === '#1C1D1F') {
        //   this.compromat.color_type = 1;
        // } else if (this.compromat.color === '#0FD900') {
        //   this.compromat.color_type = 2;
        // } else if (this.compromat.color === '#00C1FC') {
        //   this.compromat.color_type = 3;
        // } else if (this.compromat.color === '#752CF5') {
        //   this.compromat.color_type = 4;
        // } else if (this.compromat.color === '#F7002A') {
        //   this.compromat.color_type = 5;
        // } else if (this.compromat.color === '#FC4700') {
        //   this.compromat.color_type = 6;
        // }
      }
    }));

    this.subscriptions.add(this.service.click.subscribe(boolean => {
      if (!this.compromat.blocks.length) {
        this.btnSpinner.hide();
        return;
      }
      this.compromat.blocks.map((block: any) => {
        block.dirt_id = +this.compromat.id;
        if (!block.hasOwnProperty('is_color') && (block.type === 3 || block.type === 4)) {
          block.is_color = false;
        }

        if (block.type !== 9 || block.type !== 10 || block.type !== 11 || block.type !== 12 || block.type !== 13) {
          // delete block.photoOptions;
          // if(typeof block.img === 'string'){
          //   delete block.img;
          // }
        }

        if (block.type === 6) {
          // console.log(block.datepick.date);
          if (block.datepick.date) {
            const date = new Date(block.datepick.date);
            const utc_timestamp = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
            const hours = block.datepick.hours * 3600 * 1000;
            const minutes = block.datepick.minutes * 60 * 1000;
            block.datetime = (utc_timestamp + hours + minutes) / 1000;
          }
        } else if (block.type === 7 || block.type === 8) {
          block.langs.map(lang => {
            block[lang.value] = [];
            lang.arr.map(inp => {
              if (inp.value) {
                block[lang.value].push(inp.value);
              }
            });
            if (!block[lang.value].length) {
              delete block[lang.value];
            } else {
              block[lang.value] = JSON.stringify(block[lang.value]);
            }
          });

        } else if (block.type === 16) {
          if (block.choice && block.choice.hasOwnProperty('id')) {
            block.similar_id = block.choice.id;
          }
        } else if (block.type === 25) {
          if (block.choice && block.choice.hasOwnProperty('id')) {
            // console.log(block);
            block.poll_id = block.choice.id;
          }
        } else if (block.type === 26) {
          if (block.choice && block.choice.hasOwnProperty('id')) {
            block.test_id = block.choice.id;
            console.log(block);
          }
        }

        for (const opt in block.content) {
          // if(block.content[opt] !== ''){
          block[opt] = block.content[opt];
          // }

        }
      });
      // let index = 0;
      this.service.blockApi(this.compromat.blocks[this.blockIndex]);
    }));

    this.subscriptions.add(
      this.service.block.subscribe((resp: any) => {
        if (resp.success) {
          if (resp.result.hasOwnProperty('id')) {
            this.compromat.blocks[this.blockIndex].id = resp.result.id;
          }
          // this.notif.change('success', resp.message);
          this.blockIndex++;
          if (this.blockIndex <= this.compromat.blocks.length - 1) {
            this.service.blockApi(this.compromat.blocks[this.blockIndex]);
          } else {
            this.btnSpinner.hide();
            this.blockIndex = 0;
            this.notif.change('success', 'Сохранение завершено');
          }
        } else {
          const data = resp.error;
          this.compromat.blocks[this.blockIndex].errors = {};
          this.notif.change('error', 'блок ' + (this.blockIndex + 1) + ': ' + data.message);
          for (const _error in data.errors) {
            this.compromat.blocks[this.blockIndex].errors[_error] = data.errors[_error];
          }
          this.blockIndex++;
          if (this.blockIndex <= this.compromat.blocks.length - 1) {
            this.service.blockApi(this.compromat.blocks[this.blockIndex]);
          } else {
            this.btnSpinner.hide();
            this.blockIndex = 0;
            this.notif.change('success', 'Сохранение завершено');
          }
        }
      })
    );
  }

}
