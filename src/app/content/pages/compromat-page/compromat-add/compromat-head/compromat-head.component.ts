import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '../../../../templates/notification/notification.service';
import {CommonService} from '../../../../../services/common.service';
import {SpinnerBtnService} from '../../../../templates/spinner-btn/spinner-btn.service';
import {DateService} from '../../../../../services/date.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {CompromatService} from '../../services/compromat.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-compromat-head',
  templateUrl: './compromat-head.component.html',
  styleUrls: ['./compromat-head.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class CompromatHeadComponent implements OnInit, OnDestroy {


  constructor(private service: CompromatService,
              private notif: NotificationService,
              public _common: CommonService,
              private btnSpinner: SpinnerBtnService,
              public date_s: DateService,
              private router: Router) {
  }

  @ViewChild('tagsInputEl') tagsInputEl: ElementRef;
  @ViewChild('similarInputEl') similarInputEl: ElementRef;
  private subscriptions = new Subscription();
  compromat: any = {};

  timeValues: any = {
    date: null,
    hours: '00',
    minutes: '00'
  };

  compromatTitle = [
    {
      selected: true,
      value: 'title_ru',
      maxlength: '140',
      text: 'рус'
    },
    {
      selected: false,
      value: 'title_ua',
      maxlength: '140',
      text: 'укр'
    }
  ];
  errors: any = {};


  checkTime(e, inp, maxval) {
    if (+inp > maxval) {
      e.target.value = maxval;
      inp = maxval;
    }
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


  ngOnInit() {
    this.subscriptions.add(this.service.compromat.subscribe(resp => {
      if (resp) {
        console.log(resp);
        this.compromat = resp.result;
        const date = new Date(this.compromat.pub_time * 1000);
        const utc_timestamp = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
        this.timeValues.date = utc_timestamp;
        const hours = date.getUTCHours();
        const minutes = date.getUTCMinutes();
        this.timeValues.hours = +hours < 10 ? '0' + hours : hours;
        this.timeValues.minutes = +minutes < 10 ? '0' + minutes : minutes;
      }
    }));


    this.subscriptions.add(this.service.click.subscribe(() => {
      if (this.timeValues.date) {
        const date = new Date(this.timeValues.date);
        const utc_timestamp = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
        const hours = this.timeValues.hours * 3600 * 1000;
        const minutes = this.timeValues.minutes * 60 * 1000;
        this.compromat.pub_time = (utc_timestamp + hours + minutes) / 1000;


        this.subscriptions.add(this.service.addCompromatApi(this.compromat).subscribe((resp: any) => {
          this.btnSpinner.hide();
          if (resp.success) {
            if (!this.compromat.hasOwnProperty('id')) {
              this.router.navigate(['/compromat/', resp.result.id]);
            }
            this.notif.change('success', resp.message);
          }
        }, error => {
          this.btnSpinner.hide();
          const data = error.error;
          this.notif.change('error', data.message);
          for (const _error in data.errors) {
            this.errors[_error] = data.errors[_error];
          }
        }));
      } else {
        this.errors.date = 'Выбертте дату';
        this.btnSpinner.hide();
      }
    }));

  }

}
