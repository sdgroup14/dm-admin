import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PagesComponent} from './pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/persons'
      },
      {
        path: 'persons',
        loadChildren: './persons-page/persons-page.module#PersonsPageModule'
      },
      {
        path: 'compromat',
        loadChildren: './compromat-page/compromat-page.module#CompromatPageModule'
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
