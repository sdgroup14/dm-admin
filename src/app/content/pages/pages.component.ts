import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SpinnerBtnService} from '../templates/spinner-btn/spinner-btn.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  tint = false;

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private spinnerBtn: SpinnerBtnService) {
  }

  ngOnInit() {
    this.subscriptions.add(this.spinnerBtn.get().subscribe(boolean => {
      this.tint = boolean;
    }));
  }

}
