import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import {PagesComponent} from './pages.component';
import {SideBarComponent} from '../templates/side-bar/side-bar.component';
import {TooltipDirective} from '../templates/tooltip/tooltip.directive';
import {TooltipComponent} from '../templates/tooltip/tooltip.component';
import {PreloaderComponent} from '../templates/preloader/preloader.component';
import {SpinnerBtnService} from '../templates/spinner-btn/spinner-btn.service';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [
    PagesComponent,
    SideBarComponent,
    TooltipDirective,
    TooltipComponent,
    PreloaderComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    PerfectScrollbarModule
  ],
  providers: [
    SpinnerBtnService
  ]
})
export class PagesModule { }
