import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-input-file-img',
  templateUrl: './input-file-img.component.html',
  styleUrls: ['./input-file-img.component.scss']
})
export class InputFileImgComponent implements OnInit {
  @Input('imageOptions') imageOptions: any;
  @Input('label') label: any;
  @Input('size') size: any;
  @Input('error') error: any;
  @Input('width') width: any;
  @Input('height') height: any;
  @Input('base64') base64: any;
  @ViewChild('inputEl') inputEl: ElementRef;
  @Output() cropIsOpen = new EventEmitter();
  @Output() cropImg = new EventEmitter();
  @Output() cropOptions = new EventEmitter();
  @Output() checkPhoto = new EventEmitter();
  hover = false;

  clear() {
    this.inputEl.nativeElement.value = '';
  }

  getPhoto(e, option) {
    this.error = null;
    const cropEvent = e;
    const file = cropEvent.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = option.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      console.log(fileExtension);
      // console.log(fileExtension);
      if (checkFormat.length) {
        if (file.size > option.maxSize) {
          this.error = 'слишком большой файл(не больше ' + option.maxSize / 1000000 + 'мб)';
        } else {
          if (checkFormat[0].toLowerCase() === 'mp4' || checkFormat[0].toLowerCase() === 'mp3') {
            // console.log('видео выходим');
            this.cropImg.emit(file);
            return;
          } else if (checkFormat[0].toLowerCase() === 'gif') {
            console.log(file)
            this.cropImg.emit(file);
            return;
          }
          const reader = new FileReader(); // CREATE AN NEW INSTANCE.
          const that = this;
          reader.onload = (_e: any) => {
            const img = new Image();
            img.src = _e.target.result;

            img.onload = function () {
              const w = img.width;
              const h = img.height;
              if (w < option.minWidth || h < option.minHeight) {
                that.error = 'min: ' + option.minWidth + 'x' + option.minHeight + 'px';
              } else {
                that.checkPhoto.emit(true);
                that.cropIsOpen.emit(true);
                that.cropImg.emit(cropEvent);
                that.cropOptions.emit({
                  type: fileExtension.toLowerCase() === 'jpg' ? 'jpeg' : 'png',
                  size: option.minWidth,
                  maintainAspectRatio: option.maintainAspectRatio,
                  aspectRatio: option.aspectRatio,
                  minWidth: option.minWidth,
                  minHeight: option.minHeight
                });
              }
            };
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.error = 'Не верный формат (' + option.formats.join(', ') + ')';
      }
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
