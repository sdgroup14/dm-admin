import {Directive, ElementRef, HostListener, Input, OnDestroy} from '@angular/core';
import {TooltipService} from './tooltip.service';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnDestroy {
  @Input('tooltipTitle') title;
timeout: any;
  @HostListener('mouseenter')
  onMouseEnter(): void {
    const info = this.element.nativeElement.getBoundingClientRect();
    info.text = this.title;
    this.timeout = setTimeout(() => {
      this.service.change(info);
    }, 1000)
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    clearTimeout(this.timeout);
    const info = this.element.nativeElement.getBoundingClientRect();
    info.text = '';
    this.service.change(info);
  }

  ngOnDestroy(): void {
    this.service.change(null);
  }

  constructor(private element: ElementRef, private service: TooltipService) {
  }

}
