import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TooltipService {
  public tooltip = new Subject();

  get(): Observable<any> {
    return this.tooltip.asObservable();
  }

  change(data): void {
    this.tooltip.next(data);
  }
  constructor() { }
}
