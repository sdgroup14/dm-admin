import {Component, OnInit} from '@angular/core';
import {TooltipService} from './tooltip.service';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  options: any = {
    top: 0,
    left: 0,
    visibility: false,
    text: '',
  };

  constructor(private service: TooltipService) {
  }

  ngOnInit() {
    this.service.get().subscribe(data => {
      if (data) {
        this.options.visibility = true;
        this.options.left = (data.width - 10) + data.left;
        this.options.top = (data.height / 2) + data.top - 11;
        this.options.text = data.text;
      } else {
        this.options.visibility = false;
      }
    });
  }

}
