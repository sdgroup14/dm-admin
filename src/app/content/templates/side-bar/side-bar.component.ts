import {Component, OnInit} from '@angular/core';
// import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  user: any = {};
  menu = [
    // {
    //   title: 'Конструктор главной',
    //   icon: 'articles',
    //   href: '/constructor',
    //   w: 20,
    //   h: 11
    // },
    // {
    //   title: 'Список Статей',
    //   icon: 'char-t',
    //   href: '/articles',
    //   w: 16.6,
    //   h: 17
    // },
    // {
    //   title: 'Список тегов',
    //   icon: 'hash',
    //   href: '/tags',
    //   w: 17,
    //   h: 18
    // },
    {
      title: 'Персоны',
      icon: 'user',
      href: '/persons',
      w: 16,
      h: 17
    },
    {
      title: 'Компромат',
      icon: 'compromat',
      href: '/compromat',
      w: 22,
      h: 16.6
    },
    // {
    //   title: 'Фото дня',
    //   icon: 'day-photo',
    //   href: '/day-photo',
    //   w: 20,
    //   h: 20
    // },
    // {
    //   title: 'Фото 360',
    //   icon: 'photo360',
    //   href: '/photo-360',
    //   w: 20,
    //   h: 20
    // },
    // {
    //   title: 'Меню',
    //   icon: 'menu',
    //   href: '/menu',
    //   w: 22,
    //   h: 10
    // },
    // {
    //   title: 'Про нас',
    //   icon: 'info',
    //   href: '/about',
    //   w: 9.6,
    //   h: 20
    // },
    // {
    //   title: 'Бегущая строка',
    //   icon: 'tickers',
    //   href: '/tickers',
    //   w: 21,
    //   h: 21
    // },
    // {
    //   title: 'Опросы',
    //   icon: 'poll',
    //   href: '/polls',
    //   w: 13,
    //   h: 20
    // },
    // {
    //   title: 'Тесты',
    //   icon: 'tests',
    //   href: '/tests',
    //   w: 21,
    //   h: 20.3
    // },
    // {
    //   title: 'Комментарии',
    //   icon: 'comment',
    //   href: '/comments',
    //   w: 25,
    //   h: 23
    // },

  ];

  logout() {
    // this._auth.logout();
    // this.router.navigate(['/login']);
  }


  constructor() {
  }

  ngOnInit() {
    // this.user = this._auth.currentUserValue.user;
    // console.log(this.user)
  }

}
