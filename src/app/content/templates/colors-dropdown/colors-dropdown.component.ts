import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-colors-dropdown',
  templateUrl: './colors-dropdown.component.html',
  styleUrls: ['./colors-dropdown.component.scss']
})
export class ColorsDropdownComponent implements OnInit {
  isOpen = false;
  color: any;
  @Input('color') set setColor(val){
    this.color = val;
  }
  @Output() colorOutput = new EventEmitter();
  colors: any = [
    {
      current: true,
      color: '#1C1D1F'
    },
    {
      current: false,
      color: '#0FD900'
    },
    {
      current: false,
      color: '#00C1FC'
    },
    {
      current: false,
      color: '#752CF5'
    },
    {
      current: false,
      color: '#F7002A'
    },
    {
      current: false,
      color: '#FC4700'
    }
  ];

  changeColor(item) {
    item.current = true;
    this.isOpen = false;
    this.color = item.color;
    this.colors.map(_it => {
      if (_it.color !== item.color) {
        _it.current = false;
      }
    });
    this.colorOutput.emit(this.color);
  }

  constructor() {
  }


  ngOnInit() {
    if (!this.color) {
      this.color = this.colors.filter(_it => {
        return _it.current === true;
      })[0].color;
    }
  }

}
