import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService} from './notification.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  height = 36;
  offset = 12;
  sum = this.height + this.offset;
  maxItems = 5;
  notifications: any = [];

  constructor(private _service: NotificationService) {
  }

  ngOnInit() {
    this.subscriptions.add(this._service.getNotification().subscribe(notif => {
      this.addNotif({
        type: notif.type,
        text: notif.message,
        top: 0
      });
    }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  deleteNotification(notif) {
    const index = this.notifications.indexOf(notif);
    this.notifications.splice(index, 1);
    setTimeout(() => {
      this.notifications.map((_notif, i) => {
        _notif.top = this.sum * i;
      });
    });

  }

  addNotif(notif) {
    if (this.notifications.length >= this.maxItems) {
      const index = this.notifications.indexOf(this.notifications[this.notifications.length - 1]);
      this.notifications.splice(index, 1);
    }

    this.notifications.map((_notif, i) => {
      _notif.top = this.sum * (i + 1);
    });

    this.notifications.unshift(notif);
  }

}
