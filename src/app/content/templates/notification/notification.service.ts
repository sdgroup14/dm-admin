import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public notification = new Subject();

  getNotification(): Observable<any> {
    return this.notification.asObservable();
  }

  change(type, message): void {
    this.notification.next({
      type: type,
      message: message
    });
  }

  constructor() {
  }
}
