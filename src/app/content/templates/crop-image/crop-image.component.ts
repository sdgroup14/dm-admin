import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageCroppedEvent} from 'ngx-image-cropper';

@Component({
  selector: 'app-crop-image',
  templateUrl: './crop-image.component.html',
  styleUrls: ['./crop-image.component.scss']
})
export class CropImageComponent implements OnInit {

  @Input('imageChangedEvent') imageChangedEvent;
  @Input('cropIsOpen') cropIsOpen;
  @Input('options') options: any;
  @Output() closeModal = new EventEmitter();
  @Output() applyCrop = new EventEmitter();
  croppedImage: any = '';

  close() {
    this.closeModal.emit(false);
  }

  apply() {
    this.close();
    this.applyCrop.emit(this.croppedImage);
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.file;
    console.log(event)
    // event.cropperPosition.x1 = 0;
    // event.cropperPosition.y1 = 0;
  }

  constructor() {
  }

  ngOnInit() {
    this.options.maintainAspectRatio = true;
  }

}
