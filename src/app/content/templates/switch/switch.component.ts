import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {
  @Input('value') value;
  @Input('background') background?: any;
  @Output() newValue = new EventEmitter();

  toggle() {
    this.value = !this.value;
    this.newValue.emit(this.value ? 1 : 0);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
