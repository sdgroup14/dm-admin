import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-basket-dropdown',
  templateUrl: './basket-dropdown.component.html',
  styleUrls: ['./basket-dropdown.component.scss']
})
export class BasketDropdownComponent implements OnInit {
  @Input('item') item;
  @Output() itemOut = new EventEmitter();
  @Output() delete = new EventEmitter();

  dropdownVisibility(item) {
    item.dropdown = true;
    this.itemOut.emit(item);
  }

  deleteItem(item) {
    this.delete.emit(item);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
