import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-langs-panel',
  templateUrl: './langs-panel.component.html',
  styleUrls: ['./langs-panel.component.scss']
})
export class LangsPanelComponent implements OnInit {
  @Input('langs') langs;
  @Output() newLangs = new EventEmitter();

  changeLang(lang) {
    lang.selected = true;
    this.langs.map(_lang => {
      if (lang.value !== _lang.value) {
        _lang.selected = false;
      }
    });
    this.newLangs.emit(this.langs);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
