import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerBtnService {
  public visibillity = new Subject();

  public show(): void {
    this.visibillity.next(true);
  }

  public hide(): void {
    setTimeout(() => {
      this.visibillity.next(false);
    }, 100);
  }


  public get(): Observable<any> {
    return this.visibillity.asObservable();
  }


  constructor() {
  }
}
