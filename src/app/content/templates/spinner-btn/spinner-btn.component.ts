import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SpinnerBtnService} from './spinner-btn.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-spinner-btn',
  templateUrl: './spinner-btn.component.html',
  styleUrls: ['./spinner-btn.component.scss']
})
export class SpinnerBtnComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Input('text') text: any;
  @Output() event = new EventEmitter();
  isLoad = false;

  spinnerShow() {
    this.event.emit(true);
    this.service.show();
  }

  constructor(private service: SpinnerBtnService) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.service.get().subscribe(boolean => {
      // console.log('spinner: ', boolean)
      this.isLoad = boolean;
    }));
  }

}
