import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-select-template',
  templateUrl: './select-template.component.html',
  styleUrls: ['./select-template.component.scss']
})
export class SelectTemplateComponent implements OnInit {
  @Input('style') style: any;
  @Input('options') selectOptions: any;
  @Input('width') width;
  @Input('height') height;
  @Input('placeholder') placeholder;

  @Input('value') set changeValue(val) {
    const option = this.selectOptions.filter(opt => {
      return +opt.value === +val;
    })[0];
    this.topValue = option.text;
  }


  @Output() choice = new EventEmitter();
  topValue: any = null;
  isOpen = false;

  onClickedOutside(e: Event) {
    this.isOpen = false;
  }

  selectHandler(option) {
    option.selected = true;
    this.isOpen = false;
    this.topValue = option.text;
    this.choice.emit(option.value);
    this.selectOptions.map(_opt => {
      if (_opt.value !== option.value) {
        _opt.selected = false;
      }
    });
  }

  constructor() {
  }

  ngOnInit() {
  }


}
