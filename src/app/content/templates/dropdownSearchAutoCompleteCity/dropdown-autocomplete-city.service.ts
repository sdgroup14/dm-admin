import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DropdownAutocompleteCityService {

  tagsApi(text, url) {
    return this.http.get(environment.apiUrl + url + '?search=' + text, {});
  }

  constructor(private http: HttpClient) {
  }
}
