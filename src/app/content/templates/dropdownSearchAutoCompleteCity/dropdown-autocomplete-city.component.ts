import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DropdownAutocompleteCityService} from './dropdown-autocomplete-city.service';
import {DateService} from '../../../services/date.service';

@Component({
  selector: 'app-dropdown-autocomplete-city',
  templateUrl: './dropdown-autocomplete-city.component.html',
  styleUrls: ['./dropdown-autocomplete-city.component.scss']
})
export class DropdownAutocompleteCityComponent implements OnInit {

  @Input('placeholder') placeholder: any;
  @Input('url') url: any;
  choise: any = {};
  @Input('value')  set choiceChange(val){
      this.choise = val;
    // setTimeout(() => {
    // console.log('val', this.choise)
    // }, 3000)

  }
  @Output() newValue = new EventEmitter();
  list: any = [];
  value: any = '';

  search() {
    // console.log()
    // if (this.value.length > 0) {
      this.service.tagsApi(this.value, this.url).subscribe((resp: any) => {
        this.list = resp.result;
      });
    // } else {
    //   this.list = [];
    // }
  }

  delete() {
    this.choise = {};
    this.newValue.emit(this.choise);
  }

  addChoice(item) {
    this.choise = item;
    this.value = '';
    this.list = [];
    this.newValue.emit(this.choise);
  }

  constructor(private service: DropdownAutocompleteCityService, public date_s: DateService) {
  }

  ngOnInit() {

  }

}
