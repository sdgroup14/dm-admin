import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-interface-tooltip',
  templateUrl: './interface-tooltip.component.html',
  styleUrls: ['./interface-tooltip.component.scss']
})
export class InterfaceTooltipComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Input('isOpen') isOpen: boolean;
  @Input('value') tooltip: any;
  @Output() newTooltip = new EventEmitter();
  @Output() closeModal = new EventEmitter();
  errors: any = {};

  close() {
    this.closeModal.emit(false);
    this.errors = {};
  }

  apply() {
    if (this.tooltip === '') {
      this.errors.tooltip = 'Вы должны указать подсказку';
      return;
    }
    this.newTooltip.emit(this.tooltip);
    this.close();
  }

  constructor() {

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
  }

}
