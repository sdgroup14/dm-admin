import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-simple-input-file-img',
  templateUrl: './simple-input-file-img.component.html',
  styleUrls: ['./simple-input-file-img.component.scss']
})
export class SimpleInputFileImgComponent implements OnInit {
  @Input('imageOptions') imageOptions: any;
  @Input('label') label: any;
  @Input('error') error: any;
  @Input('width') width: any;
  @Input('height') height: any;
  @Input('src') src: any;
  @ViewChild('inputEl') inputEl: ElementRef;
  // @Output() cropIsOpen = new EventEmitter();
  @Output() img = new EventEmitter();
  // @Output() cropOptions = new EventEmitter();
  // @Output() checkPhoto = new EventEmitter();
  hover = false;

  clear() {
    this.inputEl.nativeElement.value = '';
  }

  getPhoto(e, option) {
    this.error = null;
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = option.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (file.size > option.maxSize) {
          this.error = 'слишком большой файл(не больше ' + option.maxSize / 1000000 + 'мб)';
        } else {
          const reader = new FileReader(); // CREATE AN NEW INSTANCE.
          const that = this;
          reader.onload = (_e: any) => {
            const img = new Image();
            img.src = _e.target.result;

            img.onload = function () {
              const w = img.width;
              const h = img.height;
              if (w < option.minWidth || h < option.minHeight) {
                that.error = 'min: ' + option.minWidth + 'x' + option.minHeight + 'px';
              } else {
                that.img.emit(file);
              }
            };
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.error = 'Не верный формат (' + option.formats.join(', ') + ')';
      }
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
