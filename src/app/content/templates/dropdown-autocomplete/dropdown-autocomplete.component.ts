import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DropdownAutocompleteService} from './dropdown-autocomplete.service';
import {DateService} from '../../../services/date.service';

@Component({
  selector: 'app-dropdown-autocomplete',
  templateUrl: './dropdown-autocomplete.component.html',
  styleUrls: ['./dropdown-autocomplete.component.scss']
})
export class DropdownAutocompleteComponent implements OnInit {

  @Input('placeholder') placeholder: any;
  @Input('url') url: any;
  choise: any = {};

  @Input('value') set choiceChange(val) {
    this.choise = val;
    // setTimeout(() => {
    // console.log('val', this.choise)
    // }, 3000)

  }

  @ViewChild('boxEl') boxEl: ElementRef;
  @Output() newValue = new EventEmitter();
  list: any = [];
  value: any = '';

  search() {
    // console.log()
    // if (this.value.length > 0) {
    this.service.tagsApi(this.value.toLowerCase(), this.url).subscribe((resp: any) => {
      this.list = resp.result;
      // console.log(this.list)
    });
    // } else {
    //   this.list = [];
    // }
  }

  delete() {
    this.choise = {};
    this.newValue.emit(this.choise);
  }

  addChoice(item) {
    this.choise = item;
    this.value = '';
    this.list = [];
    this.newValue.emit(this.choise);
    setTimeout(() => {
      this.choise = item;
      this.value = '';
      this.list = [];
      this.newValue.emit(this.choise);
    }, 10);
  }

  constructor(private service: DropdownAutocompleteService, public date_s: DateService) {
  }

  ngOnInit() {

  }

}
