import {Injectable} from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  convert(timestamp) {
    return moment(timestamp * 1000).format('ll');
  }

  convertMin(timestamp) {
    return moment(timestamp * 1000).format('L');
  }


  convertWithTime(timestamp) {
    return moment(timestamp * 1000).format('lll');
  }

  convertWithTimeUTC(timestamp) {
    return moment(timestamp * 1000).utc().format('lll');
  }


  constructor() {
    moment.locale('ru');
  }
}

