import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  clearField(key, errors) {
    delete errors[key];
  }

  textareaHandler(e, textarea) {
    textarea.count = e.target.value.length;
  }

  getValueCount(text) {
    if (text) {
      return text.length;
    }
    return 0;
  }

  dropdownVisibility(target, items) {
    items.map((_tag) => {
      if (target.id !== _tag.id) {
        _tag.dropdown = false;
      }
    });
  }

  constructor() {
  }
}
