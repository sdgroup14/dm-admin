import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {
  }

  public transform(url) {
    if (typeof url !== 'string') {
      return this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(url));
    }
    return url;
  }
}
