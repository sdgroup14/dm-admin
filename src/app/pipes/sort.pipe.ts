import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(array: Array<string>, args: string): Array<string> {

    array.sort((a: any, b: any) => {
      if (a.order_number < b.order_number) {
        return -1;
      } else if (a.order_number > b.order_number) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
