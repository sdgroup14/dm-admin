import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NotificationService} from './content/templates/notification/notification.service';
import {TooltipService} from './content/templates/tooltip/tooltip.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonService} from './services/common.service';
import {DateService} from './services/date.service';
import {HttpClientModule} from '@angular/common/http';
import {NotificationComponent} from './content/templates/notification/notification.component';

@NgModule({
  declarations: [
    AppComponent,
    NotificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    NotificationService,
    TooltipService,
    CommonService,
    DateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
