var gulp = require('gulp'),
    notify = require("gulp-notify"),
    spritesmith = require('gulp.spritesmith'),
    opts = { /* plugin options */ },
    svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace');


gulp.task('sprite', function() {
    var spriteData =
        gulp.src('sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: '/assets/img/sprite.png',
                cssName: '_png_sprite.scss',
                algorithm: 'binary-tree',
            }));

    spriteData.img.pipe(gulp.dest('src/assets/img')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('src/assets/scss/libs/'))
      // .pipe(notify("Done Png Sprite")); // путь, куда сохраняем стили
});


gulp.task('svg', function () {
  return gulp.src('svg-sprite/*.svg')
  // minify svg
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    // remove all fill, style and stroke declarations in out shapes
    .pipe(cheerio({
      run: function ($) {
        $('[fill]').removeAttr('fill');
        $('[stroke]').removeAttr('stroke');
        $('[style]').removeAttr('style');
        $('[class]').removeAttr('class');
        $('style').remove();
      },
      parserOptions: {xmlMode: true}
    }))
    // cheerio plugin create unnecessary string '&gt;', so replace it.
    .pipe(replace('&gt;', '>'))
    // build svg sprite
    .pipe(svgSprite({
    mode: {
      symbol: {
        sprite: "../../../src/assets/img/sprite.svg",
        render: {
          scss: {
            dest:'../../../src/assets/scss/libs/_sprite.scss',
            template: "svg-sprite/scss/sprite-template.scss"
          }
        }
      }
    }
  }))
    // .pipe(notify("Done Svg Sprite"));
    .pipe(gulp.dest('svg-sprite/done/'))
    // .pipe(notify("Done Svg Sprite"));
});

